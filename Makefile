bld = build
src = src
inc = include

box = $(bld)/box.o $(bld)/boxrun.o $(bld)/boxinstall.o $(bld)/config.o $(bld)/utils.o
installer = $(bld)/install.o $(bld)/cli.o $(bld)/cliinstall.o
runner = $(bld)/run.o $(bld)/cli.o $(bld)/clirun.o

main: installer runner

all: installer runner lib

installer: $(bld)/box-install

runner: $(bld)/box-run

lib: $(bld)/libbox.a

$(bld)/box-install: $(box) $(installer)
	cc $^ -o $@

$(bld)/box-run: $(box) $(runner)
	cc $^ -o $@

$(bld)/libbox.a: $(box)
	ar -r $@ $^

$(bld)/%.o: $(src)/%.c
	@mkdir -p $(bld)
	cc -c -I $(inc) $^ -o $@

install: installer runner
	mkdir -p $(DESTDIR)/usr/bin
	cp $(bld)/box-install $(DESTDIR)/usr/bin
	cp $(bld)/box-run $(DESTDIR)/usr/bin
	cp sh/box-aur $(DESTDIR)/usr/bin
	chmod u+s $(DESTDIR)/usr/bin/box-run
	mkdir -p $(DESTDIR)/etc
	cp -rT config $(DESTDIR)/etc/box-installer
	mkdir -p $(DESTDIR)/usr/share/vim/vimfiles/syntax $(DESTDIR)/usr/share/vim/vimfiles/ftdetect
	cp vim/syntax.vim $(DESTDIR)/usr/share/vim/vimfiles/syntax/boxinstaller.vim
	cp vim/ftdetect.vim $(DESTDIR)/usr/share/vim/vimfiles/ftdetect/boxinstaller.vim

uninstall:
	rm $(DESTDIR)/usr/bin/box-install
	rm $(DESTDIR)/usr/bin/box-run
	rm $(DESTDIR)/usr/bin/box-aur
	rm -r $(DESTDIR)/etc/box-installer
	rm $(DESTDIR)/usr/share/vim/vimfiles/syntax/boxinstaller.vim $(DESTDIR)/usr/share/vim/vimfiles/ftdetect/boxinstaller.vim
	rmdir --ignore-fail-on-non-empty $(DESTDIR)/usr/share/vim/vimfiles/syntax $(DESTDIR)/usr/share/vim/vimfiles/ftdetect $(DESTDIR)/usr/share/vim/vimfiles $(DESTDIR)/usr/share/vim

clean:
	rm -rf $(bld)

.PHONY: main all installer runner lib install uninstall clean
