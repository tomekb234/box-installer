# Box installer

**Warning**: This software is highly experimental and should not be used in production environments.

This program lets you install and run Arch Linux packages in semi-isolated `chroot` environments called *boxes*. Each box contains all files of some set of packages, typically one package of interest with all dependencies, which are unaffected by upgrades and package removals on the host system. This can potentially safeguard a given package from dependency breakages after host system upgrades. Programs run from boxes have access to the host's system interfaces (`/proc`, `/run`, etc.), the `/home` directory, and some essential configuration files in the `/etc` directory. This access can be easily extended or reduced, individually for each box. Due to the large total size of all dependencies of a typical Arch Linux package, it is only practical to keep a few boxes for important and often-used programs.

## Building

- `make` - Build the `box-install` and `box-run` programs.
- `make installer` - Build `box-install` only.
- `make runner` - Build `box-run` only.
- `make lib` - Build the `libbox` library.
- `sudo make install` - Install the programs at `/usr/bin` and the default configuration files at `/etc/box-installer`.
- `sudo make uninstall` - Uninstall the programs and all (including user-made) configuration files.

## Command line interface

Box installer provides two programs, `box-install` and `box-run`. The first one manages boxes and packages, and the second runs programs installed in boxes. An auxiliary script `box-aur` for installing packages from Arch User Repository is also provided. The options for these programs are explained below.

### `box-install`

This program must be run by root. You need to provide the name of the box, to which it will apply. If no options are specified, the `box-install` tries to create a new box with a given name and install a package with the same name using `pacman` inside this box. To change this default behaviour you can add the options listed below.

`box-install` options | Description
----------------------|------------
`--create` or `-c` | Creates a box with the specified name and installs only the default dependencies (specified in the configuration file). Does not try to install the package with the name of the box. Example usage: `box-install -c new_box`.
`--remove` or `-r` | Removes a box with the specified name. Example usage: `box-install -r box_to_remove`.
`--install` or `-i` | Installs a given package in the specified box. Example usage: `box-install -i package_I_want my_box`.
`--install-local` or `-I` | Installs a package from a file in the specified box. Example usage: `box-install -I my/local/package my_box`.
`--uninstall` or `-u` | Uninstalls a given package from the specified box. Example usage: `box-install -u package_to_remove my_box`.
`--upgrade` or `-g` | Upgrades all packages in the specified box. Example usage: `box-install -g my_box`.
`--pacman` or `-p` | Runs a `pacman` command in the specified box. Treats all arguments that come after as arguments for `pacman`. Example usage: `box-install my_box -p --sync --sysupgrade`.
`--noconfirm` or `-y` | Automatically selects the default response in case of a question. Example usage: `box-install -y my_box`.
`--` | Treats the next argument as the name of the box. It allows to use name starting with `'-'` character that would otherwise be treated as an argument. Example usage: `box-install -- -my_box`.
`--help` or `-h` | Displays the help message (similar to this table but more concise). Example usage: `box-install -h`.

### `box-run`

This program can be run by regular users (and must have `setuid` permission flag). You need to provide the name of the box, to which it will apply. If no options are specified, `box-run` tries to run the box's default program. All arguments specified after the name of the box are treated as the arguments for the program being run. To change this default behaviour you can add the options listed below.

`box-run` options | Description
------------------|------------
`--working-dir` or `-w` | Overrides the working directory from the configuration file. Example: `box-run -w /my/new/working/dir my_box`.
`--exec-path` or `-e` | Overrides the executable path. Specify it as the first argument after the name of the box. Example: `box-run -e my_box /path/to/other/exec`.
`--` | Treats the next argument as the name of the box. It allows to use name starting with `'-'` character that would otherwise be treated as an argument. Example usage: `box-run -- -my_box`.
`--help` or `-h` | Displays the help message (similar to this table but more concise). Example usage: `box-run -h`.

### `box-aur`

This script installs or upgrades a package from Arch User Repository. It may *not* be run as root. The first argument is the name of the box. The second optional argument is the name of the package, which is assumed to be the same as the box's name if not specified. A few options may also be used before the first argument.

`box-aur` options | Description
------------------|------------
`--upgrade` or `-g` | Skips the creation of a new box.
`--nodeps` or `-d` | Ignores missing build dependencies.
`--noconfirm` or `-y` | Automatically selects the default response in case of a question.
`--` | Treats the next argument as the name of the box, even if it starts with `'-'`.

The script uses `git` to download or update the package and `makepkg` to build it. The dependencies for the build process are temporarily installed, unless `--nodeps` od `-d` is used. This option may be useful, because `makepkg` assumes that all runtime dependencies are also build dependencies, which is usually not the case. It is also a workaround for a problem when an AUR package depends on another AUR package, which is installed in a box and not on the host, but it is not an actual build dependency.

## Configuration

The main configuration file is `/etc/box-installer/config`, and it may be extended by `/etc/box-installer/boxes/[name]` file when a box with `[name]` is being installed or run.

The configuration files are read line-by-line. Empty lines and lines starting with `#` are ignored. Every proper line is either a single keyword or starts with a single keyword followed by a space and an argument, which spans to the end of the line and may contain any characters (including spaces). The following table lists all recognized configuration options.

Keyword | Argument | Description
--------|----------|------------
`path` | Host directory path | Sets the box's path as the argument.
`path-prefix` | Host directory path | Sets the box's path as the argument followed by the box's name.
`exec` | Box file path | Sets the box's default executable as the argument.
`exec-prefix` | Box directory path | Sets the box's default executable as the argument followed by the box's name.
`arg` | Any string | Adds an argument to the box's default executable.
`workdir` | Box directory path | Sets the box's default working directory as the argument.
`copy-env` | None | Makes the box copy the host's environment. Overrides `empty-env`.
`empty-env` | None | Makes the box start with an empty environment. Overrides `copy-env`.
`env` | Any string | Adds a variable to the box's environment. If the argument does not contain the `=` character, it is treated as a name of the variable to be copied from the host.
`bind` | Host directory path | Adds a directory binding between the host and the box. The corresponding box path is the same by default.
`sync` | Host file path | Adds a file synchronization between the host and the box. The corresponding box path is the same by default. See the "Technical details" section for discussion about the difference between binding and synchronization.
`target` | Box path | Sets the corresponding box path of the last `bind` or `sync` option.
`install-only` | None | Marks the last `bind` or `sync` option as to be used only during the box's installation.
`dont-bind` | Host directory path | Removes a previously added directory binding. Useful in box-specific overrides of the global configuration.
`dont-sync` | Host file path | Removes a previously added file synchronization.
`dep` | Package name | Adds a package to be installed as an implicit dependency during the box's creation.

All paths must be absolute.

The default global configuration file provided with this program:

- sets the box's path prefix to `/opt/boxes`,
- sets the box's default executable prefix to `/usr/bin`,
- sets the box's default working directory to `/`,
- makes the box copy the host's environment,
- adds a binding from `/` to `/host` (this lets the user browse all host files from the box),
- adds bindings for system interfaces (`/sys`, `/run`, etc.),
- adds a binding for `/home`,
- adds an install-only binding for `/var/lib/pacman/sync` (see "Technical details" for explanation),
- adds file synchronization for a few essential configuration files (such as `/etc/passwd`), which may be necessary for the programs in the box to run correctly,
- adds a subset of `base` Arch Linux metapackage as a default dependency.

These options may be easily overridden for a specific box by creating a `/etc/box-installer/boxes/[box-name]` file, which will be read after the main configuration file.

## Limitations

- A box name cannot contain the `'/'` character or be equal to `"."` or `".."`.
- The configuration for a box should not be changed while it is running.
- Boxes shouldn't be run from inside of the box environment (box nesting). It is technically possible (and should work), but leads to a bind explosion (multiplication of the number of bindings).

## Technical details

### Box package management

The packages in a box are managed by the host's `pacman` command with `--root` option. A box does not need its own version of `pacman`.

Each box keeps its own `/var/lib/pacman/local` database, but shares the `/var/lib/pacman/sync` database with the host, so that synchronization only needs to be performed on the host.

The package installation process may modify the configuration files shared with the host (for example by adding a new user to `/etc/passwd`), and in this case the changes are copied back to the host.

### Runtime (un)mounting

Boxes are run in a `chroot` environment, but no other means of isolation (such as kernel namespaces) are used. When a first instance of a box is started, the box is mounted. This involves binding the box's root to the runtime directory (located at `/run/box-installer/runtime/[box-name]`), followed by performing all bindings and synchronizations defined in configuration files. All subsequent instances of the same box use the already mounted environment.

After setting up the box environment and starting the desired program, the supervisor process (e.g. `box-run`) waits for all children processes to terminate. After that, a cleanup is performed. In particular, if no more instances of the box are active, the box is unmounted (which is the opposite process to mounting).

### Locks

Box instances are tracked via lockfiles, which work as read-write locks. Each box has a corresponding lockfile located at `/run/box-installer/locks/[box-name]`. This file contains a 32-bit unsigned integer counting the active instances of the given box. The maximum value of $`2^{32}-1`$ is reserved and means that an exclusive lock is held. A lack of a lockfile is semantically equal to a file containing 0. The access to lockfiles is controlled using advisory file locking (using only exclusive mode).

### Bindings / Synchronization

Two mechanisms of sharing files with the host are provided: *bindings* and *synchronization*.

Binding means mounting files or directories from the host to the box using bind mount. This allows any changes on one side of the binding to be immediately reflected on the other. All bindings are performed recursively and their propagation mode is set to `SLAVE`. In order to avoid loops (bind explosion), the box-installer runtime directory (and its subtree) is always skipped if used as a binding source.

Synchronization means copying individual files to the box during mounting and back to the host during unmounting. The actual copying is only performed if the target file modification time is earlier than source file modification time, or if the target file doesn't exist. The owner and permissions are preserved. This mechanism should be used instead of file bind mounting, because a bind-mounted file may not be renamed or removed, and some programs modify files by creating a new version first and then swapping it with the old one. The drawback of this approach is that synchronization is only performed at the beginning and end of the box run time, so any changes in between are not immediately reflected (unlike with bind mounting). If the file is modified on both sides during the box run time, no copy is performed and a warning is issued. For this reason, only rarely-modified files such as those in `/etc` should be synchronized.
