#ifndef BOX_H
#define BOX_H

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include <signal.h>

#include "config.h"

// data stored while the box is running
struct box_runtime {
    char* box_name;
    char* box_root;
    bool install;
    size_t nfilesyncs;
    struct binding* filesyncs;
    struct timespec timestamp;
    sigset_t saved_signal_mask;
};

struct binding {
    char* source;
    char* target;
};

struct lock {
    int fd;
    uint32_t old_value;
    uint32_t new_value;
};

// the path where boxes are mounted
extern const char* runtime_dir_path;

// the path where lockfiles are stored
extern const char* lockfiles_dir_path;

// returns 0 if the name for a box is valid;
// the name may not contain the '/' character or be equal to "." or ".."
int check_box_name(const char* name);

// acquires a lock for a given box and allocates a lock structure;
// if `exclusive` is true, only one lock is allowed for this box at the same time;
// if `acquire` is true, increments lock counter, decrements otherwise;
// actual value saving is delayed until release;
// assumes that `box_name` is valid
int get_lock(struct lock** lock_ptr, const char* box_name, bool exclusive, bool acquire);

// releases a lock, saves new counter value if `save` is true, and frees the lock structure
int release_lock(struct lock** lock_ptr, bool save);

// creates a list of mount bindings with absolute target paths from box's config;
// if `runtime` is false, uses regular box path;
// if `runtime` is true, uses runtime path, and the first binding is the box directory (with target in canonical form);
// if `filesync` is false, skips filesync bindings;
// if `filesync` is true, skips regular bindings;
// assumes that `config->box_name` is valid
void make_bindings(size_t* nbindings, struct binding** bindings, struct box_config* config, bool runtime, bool install, bool filesync);

// frees the mount binding list
void free_bindings(size_t nbindings, struct binding* bindings);

// sets up an environment necessary for running a process in a box and allocates a runtime structure;
// assumes that `config->box_name` is valid
int start_box(struct box_runtime** runtime_ptr, struct box_config* config, bool install);

// prepares the child process to work in the box environment
int prepare_child_process(struct box_runtime* runtime);

// cleans up the environment necessary for running a process in a box and frees the runtime structure;
// returns the exit code of the child process with pid equal to `main_child_pid`, or 0 if no such process was found
int stop_box(struct box_runtime** runtime_ptr, pid_t main_child_pid);

#endif
