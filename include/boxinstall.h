#ifndef BOXINSTALL_H
#define BOXINSTALL_H

#include <stddef.h>

#include "config.h"

// creates a new box with all files and directories necessary to mount;
// installs the default dependencies specified in config;
// fails if the box already exists
int create_box(struct box_config* config, bool noconfirm);

// runs any pacman command in a box
int run_pacman_in_box(struct box_config* config, size_t nargs, const char* const* args, bool noconfirm);

// installs or upgrades a package from the repository in a box
int install_in_box(struct box_config* config, const char* pkgname, bool noconfirm);

// installs or upgrades a package from a file in a box
int install_local_in_box(struct box_config* config, const char* filename, bool noconfirm);

// uninstalls a package from a box (this reverses `install_in_box`)
int uninstall_from_box(struct box_config* config, const char* pkgname, bool noconfirm);

// upgrades all packages in a box
int upgrade_in_box(struct box_config* config, bool noconfirm);

// deletes a box and all its files;
// fails if the box does not exist
int remove_box(struct box_config* config);

#endif
