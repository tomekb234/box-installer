#ifndef BOXRUN_H
#define BOXRUN_H

#include <stddef.h>

#include "config.h"

// runs a box with the given configuration, optional custom binary path, optional additional arguments, and optional custom working directory;
// if the binary path is not specified, the additional arguments are appended to the arguments from the configuration,
// otherwise the additional arguments override the arguments from the configuration;
// returns the exit code of the process, or -1 in case of an error
int run_box(struct box_config* config, const char* exec_path_override, size_t nargs, const char* const* args, const char* workdir_override);

#endif
