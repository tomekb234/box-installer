#ifndef CLI_H
#define CLI_H

// print an error that an option was specified multiple times
void print_duplicate(const char* name, const char* option);

// print an error that no arguments where passed
void print_no_args(const char* name);

// print an error that an option was left without a value
void print_missing_value(const char* name, const char* option);

// print an error that the specified option is not known
void print_unknown_option(const char* name, const char* option);

// print how to view help
void print_use_help(const char* name);

// checks for missing values or duplicates; returns 0 if none
int check_option_errors(int cnt, int argc, const char* val, const char* name, const char* option);

#endif
