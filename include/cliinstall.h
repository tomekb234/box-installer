#ifndef CLIINSTALL_H
#define CLIINSTALL_H

#include <stddef.h>
#include <stdbool.h>

enum install_action {
    CREATE,
    INSTALL,
    INSTALL_LOCAL,
    CREATE_AND_INSTALL,
    UNINSTALL,
    REMOVE,
    UPGRADE,
    PACMAN
};

struct install_args {
    enum install_action action;
    const char* box_name;
    const char* package_name;
    const char* file_name;
    bool noconfirm;
    size_t nargs;
    const char* const* args;
};

// parses the arguments for the install program and fills in the `install_args` struct
int parse_install_args(int argc, const char* const* argv, struct install_args* args);

#endif
