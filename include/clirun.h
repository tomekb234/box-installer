#ifndef CLIRUN_H
#define CLIRUN_H

#include <stddef.h>

struct run_args {
    const char* box_name;
    const char* exec_path;
    const char* working_dir;
    size_t nargs;
    const char* const* args;
};

// parses the arguments for the run program and fills in the `run_args` struct
int parse_run_args(int argc, const char* const* argv, struct run_args* args);

#endif
