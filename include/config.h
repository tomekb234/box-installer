#ifndef CONFIG_H
#define CONFIG_H

#include <stddef.h>
#include <stdbool.h>

struct box_config {
    char* box_name;
    char* box_path;
    char* exec_path;
    size_t nargs;
    char** args;
    char* workdir;
    bool copy_env;
    size_t env_size;
    char** env;
    size_t nbindings;
    struct conf_binding* bindings;
    size_t ndeps;
    char** deps;
};

struct conf_binding {
    char* source;
    char* target;
    bool filesync;
    bool install_only;
};

// the path of the configuration directory
extern const char* config_dir_path;

// the name of the main configuration file
extern const char* config_file_name;

// the name of the directory with box-specific configuration files
extern const char* boxconfig_dir_name;

// creates a new empty box configuration
struct box_config* new_config();

// updates the box configuration by reading a configuration file;
// non-existent or unreadable files are ignored;
// the `config->box_name` field must be non-null
void read_config_file(struct box_config* config, const char* path);

// creates a box configuration by reading configuration files
// at standard paths based on box name
struct box_config* load_config(const char* box_name);

// frees the box configuration
void free_config(struct box_config* config);

#endif
