#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>

// allocates a new string, copies `src` into it, and returns the pointer
char* alstrcpy(const char* src);

// reallocates `*str`, copies `src` into it, and returns the length of `src`
size_t restrcpy(char** str, const char* src);

// reallocates `*str`, copies src to `*str + pos`, and returns the length of `src`
size_t restrcpy_at(char** str, size_t pos, const char* src);

// allocates a new string, writes with the given format into it, and returns the pointer
char* alsprintf(const char* format, ...);

// reallocates `*str`, writes with the given format into it, and returns the result of `sprintf`
int resprintf(char** str, const char* format, ...);

// prints a message to `stderr` followed by '\n'
void printf_err(const char* format, ...);

// prints a message to `stderr` followed by the system error description (from errno) and '\n'
void printf_syserr(const char* format, ...);

// creates all necessary parent directories of the given path
int mkdir_parents(const char* path);

// creates a directory with all necessary parents
int mkdir_all(const char* dir);

// creates an empty file if it does not exist
int touch(const char* file);

// creates an empty file with all necessary parent directories
int mkdir_all_and_touch(const char* file);

// recursively removes a directory with all its contents
int rmdir_all(const char* path);

// copies a file from `source` to `target`;
// preserves the owner, group and permissions;
// does not preserve the timestamps;
// this operation is atomic and never changes the target in case of failure
int copy_file(const char* source, const char* target);

#endif
