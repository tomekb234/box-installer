#define _GNU_SOURCE

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/mount.h>
#include <sys/wait.h>
#include <sys/prctl.h>

#include "box.h"
#include "config.h"
#include "utils.h"

const static uint32_t exclusive_lock_value = 0xffffffff;
const static uint32_t max_lock_counter_value = 0xfffffffe;

const char* runtime_dir_path = "/run/box-installer/runtime"; //must be in canonical form, different from "/"
const char* lockfiles_dir_path = "/run/box-installer/locks";
const char* global_lockfile_name = "../global_lock"; //this file shouldn't be placed in "locks" directory

int check_box_name(const char* name) {
    if (strchr(name, '/') || !strcmp(name, ".") || !strcmp(name, "..")) {
        printf_err("Invalid box name");
        return -1;
    }
    return 0;
}

static int acquire_box_lock(const char* box_name) {
    char *lockfile_path = alsprintf("%s/%s", lockfiles_dir_path, box_name);
    int fd;

    //open file, creating it if needed
    if (mkdir_all(lockfiles_dir_path)) {
        printf_err("Failed to make the lockfile directory");
        free(lockfile_path);
        return -1;
    }
    if ((fd = open(lockfile_path, O_RDWR | O_CREAT, 0644)) == -1) {
        printf_syserr("Failed to open or create lock file %s", lockfile_path);
        free(lockfile_path);
        return -1;
    }

    //lock
    if (flock(fd, LOCK_EX) == -1) {
        printf_syserr("Failed to flock() lock file %s", lockfile_path);
        close(fd);
        free(lockfile_path);
        return -1;
    }

    free(lockfile_path);
    return fd;
}

static int get_box_lock_value(int fd, uint32_t* value) {
    if (lseek(fd, 0, SEEK_SET) == -1) {
        printf_syserr("Failed to seek() in a lock file");
        return -1;
    }
    int bytes_read = read(fd, value, 4);
    if (bytes_read == -1) {
        printf_syserr("Failed to read from a lock file");
        return -1;
    }
    else if (bytes_read == 0)
        *value = 0;
    return 0;
}

static int set_box_lock_counter(int fd, uint32_t value) {
    if (lseek(fd, 0, SEEK_SET) == -1) {
        printf_syserr("Failed to seek() in a lock file");
        return -1;
    }
    if (write(fd, &value, 4) != 4) {
        printf_syserr("Failed to write to a lock file");
        return -1;
    }
    return 0;
}


int get_lock(struct lock** lock_ptr, const char* box_name, bool exclusive, bool acquire) {
    //allocate structure
    struct lock* lock = *lock_ptr = malloc(sizeof(struct lock));

    //open and acquire lock file
    if ((lock->fd = acquire_box_lock(box_name)) == -1) {
        printf_err("Failed to acquire the box lock");
        goto ERROR_FREE;
    }

    //get current value
    if (get_box_lock_value(lock->fd, &lock->old_value)) {
        printf_err("Failed to read the lock counter");
        goto ERROR_CLOSE;
    }

    //check whether operation is possible, note down new value
    if (exclusive) {
        if (acquire && lock->old_value != 0) {
            printf_err("Error: Cannot acquire an exclusive lock");
            goto ERROR_CLOSE;
        }
        if (!acquire && lock->old_value != exclusive_lock_value) {
            printf_err("Error: Cannot release an exclusive lock");
            goto ERROR_CLOSE;
        }
        lock->new_value = acquire ? exclusive_lock_value : 0;
    }
    else {
        if (acquire && lock->old_value >= max_lock_counter_value) {
            printf_err("Error: Cannot acquire a shared lock");
            goto ERROR_CLOSE;
        }
        if (!acquire && (lock->old_value == 0 || lock->old_value == exclusive_lock_value)) {
            printf_err("Error: Cannot release a shared lock");
            goto ERROR_CLOSE;
        }
        lock->new_value = acquire ? (lock->old_value + 1) : (lock->old_value - 1);
    }

    return 0;

    //rollback in case of error
ERROR_CLOSE:
    close(lock->fd);

ERROR_FREE:
    free(lock);
    *lock_ptr = NULL;

    return -1;
}

int release_lock(struct lock** lock_ptr, bool save) {
    struct lock* lock = *lock_ptr;

    //save value
    if (save) {
        if (set_box_lock_counter(lock->fd, lock->new_value)) {
            printf_err("Failed to set the lock counter");
            return -1;
        }
    }

    //release lock file
    close(lock->fd);

    //free structure
    free(lock);
    *lock_ptr = NULL;

    return 0;
}

static int read_canonical_mountpoints(char ***mountpoint_tab_ptr, size_t *mountpoint_num_ptr, const char* white_prefix, const char* black_prefix, bool include_exact_match) {
    size_t white_prefix_len = white_prefix ? strlen(white_prefix) : 0;
    size_t black_prefix_len = black_prefix ? strlen(black_prefix) : 0;

    //open "/proc/self/mounts" file
    FILE *file = fopen("/proc/self/mounts", "r");
    if (file == NULL) {
        printf_syserr("Failed to open /proc/self/mounts file");
        return -1;
    }

    //list all interesting mountpoints
    size_t mountpoints_num = 0, line_len;
    char **mountpoint_tab = NULL, *line = NULL;
    while (getline(&line, &line_len, file) != -1) {
        char *token;
        if(strtok(line, " ") == NULL || (token = strtok(NULL, " ")) == NULL)
            continue;
        if ((white_prefix && strncmp(token, white_prefix, white_prefix_len)) || (black_prefix && !strncmp(token, black_prefix, black_prefix_len)))
            continue;
        if (!include_exact_match && strlen(token) == white_prefix_len)
            continue;
        mountpoint_tab = realloc(mountpoint_tab, ++mountpoints_num * sizeof(char*));
        mountpoint_tab[mountpoints_num - 1] = alstrcpy(token);
    }

    //cleanup
    free(line);
    fclose(file);

    *mountpoint_num_ptr = mountpoints_num;
    *mountpoint_tab_ptr = mountpoint_tab;

    return 0;
}

static int bind_mount(const char* source, const char* target) {
    if (mount(source, target, "", MS_BIND, "")) {
        printf_syserr("Failed to mount binding from %s to %s", source, target);
        return -1;
    }
    if (mount("", target, "", MS_SLAVE, "")) {
        printf_syserr("Failed to set binding parameters for %s", target);
        umount2(target, UMOUNT_NOFOLLOW | MNT_DETACH);
        return -1;
    }
    return 0;
}

//this function may leave some bindings in case of error
static int recursive_bind(const char* source, const char* target) {
    //get canonical source path
    char *canonical_source = realpath(source, NULL);
    if (canonical_source == NULL) {
        printf_syserr("Failed to canonicalize source path %s", source);
        return -1;
    }
    size_t canonical_source_len = strlen(canonical_source);

    //get mountpoint list
    size_t source_mountpoints_num;
    char **source_mountpoint_tab;
    if (read_canonical_mountpoints(&source_mountpoint_tab, &source_mountpoints_num, canonical_source, runtime_dir_path, false)) {
        printf_err("Failed to read the mountpoints");
        goto ERROR_FREE_CANONICAL_PATH;
    }

    //mount main dir
    if (bind_mount(source, target)) {
        printf_err("Failed to make the main binding");
        goto ERROR_FREE_MOUNTPOINTS;
    }

    //mount the rest in order
    char *target_path = NULL;
    for (size_t i = 0; i < source_mountpoints_num; i++) {
        resprintf(&target_path, "%s/%s", target, source_mountpoint_tab[i] + canonical_source_len);
        if (bind_mount(source_mountpoint_tab[i], target_path)) {
            printf_err("Failed to make a recursive binding");
            goto ERROR_FREE_TARGET_PATH;
        }
    }

    //cleanup
    free(target_path);
    for (size_t i = 0; i < source_mountpoints_num; i++)
        free(source_mountpoint_tab[i]);
    free(source_mountpoint_tab);
    free(canonical_source);

    return 0;

    //rollback in case of error
ERROR_FREE_TARGET_PATH:
    free(target_path);

ERROR_FREE_MOUNTPOINTS:
    for (size_t i = 0; i < source_mountpoints_num; i++)
        free(source_mountpoint_tab[i]);
    free(source_mountpoint_tab);

ERROR_FREE_CANONICAL_PATH:
    free(canonical_source);

    return -1;
}

static void recursive_umount(const char* canonical_path) {
    //get mountpoint list
    size_t canonical_moutpoints_num;
    char **canonical_mountpoint_tab;
    if (read_canonical_mountpoints(&canonical_mountpoint_tab, &canonical_moutpoints_num, canonical_path, NULL, true)) {
        printf_err("Failed to read the mountpoints");
        return;
    }

    //umount in reverse order
    for (size_t i = 0; i < canonical_moutpoints_num; i++)
        if (umount2(canonical_mountpoint_tab[canonical_moutpoints_num - 1 - i], UMOUNT_NOFOLLOW | MNT_DETACH))
            printf_syserr("Failed to unmount %s", canonical_mountpoint_tab[canonical_moutpoints_num - 1 - i]);

    //cleanup
    for (size_t i = 0; i < canonical_moutpoints_num; i++)
        free(canonical_mountpoint_tab[i]);
    free(canonical_mountpoint_tab);
}

static bool is_timestamp_earlier(struct timespec a, struct timespec b, bool or_equal) {
    if (a.tv_sec != b.tv_sec)
        return a.tv_sec < b.tv_sec;
    return or_equal ? (a.tv_nsec <= b.tv_nsec) : (a.tv_nsec < b.tv_nsec);
}

static int syncfile_to_box(const char* source, const char* target, struct timespec* timestamp) {
    struct stat st_src;
    struct stat st_tar;
    if (stat(source, &st_src)) {
        printf_syserr("Failed to stat host syncfile %s", source);
        return -1;
    }

    if (stat(target, &st_tar) || is_timestamp_earlier(st_tar.st_ctim, st_src.st_ctim, false)) {
        if (copy_file(source, target)) {
            printf_err("Failed to synchronize from %s to %s", source, target);
            return -1;
        }
        if (stat(target, &st_tar)) {
            printf_syserr("Failed to stat box syncfile %s after synchronization", target);
            return -1;
        }
    }

    if (is_timestamp_earlier(*timestamp, st_tar.st_ctim, false))
        *timestamp = st_tar.st_ctim;

    return 0;
}

static void syncfile_to_host(const char* source, const char* target, struct timespec timestamp) {
    struct stat st_src;
    struct stat st_tar;
    if (stat(source, &st_src)) {
        printf_syserr("Failed to stat box syncfile %s", source);
        return;
    }

    if (is_timestamp_earlier(st_src.st_ctim, timestamp, true))
        return;

    if (stat(target, &st_tar)) {
        printf_syserr("Failed to stat host syncfile %s", target);
        return;
    }

    if (is_timestamp_earlier(timestamp, st_tar.st_ctim, false)) {
        printf_err("Warning: The files %s and %s were both modified. Synchronization skipped.", source, target);
        return;
    }

    if (copy_file(source, target)) {
        printf_err("Failed to synchronize from %s to %s", source, target);
        return;
    }
}

static int prepare_global_runtime_directory() {
    struct lock* global_lock;

    //make runtime directory
    if (mkdir_all(runtime_dir_path)) {
        printf_err("Failed to make the global runtime directory");
        return -1;
    }

    //get global lock
    if (get_lock(&global_lock, global_lockfile_name, false, true)) {
        printf_err("Failed to get the global lock");
        return -1;
    }

    //self-bind if first run
    if (global_lock->old_value == 0 && bind_mount(runtime_dir_path, runtime_dir_path)) {
        printf_err("Failed to self-bind the global runtime directory");
        goto ERROR_RELEASE;
    }

    //save lock value
    if (global_lock->old_value == 0) {
        if (release_lock(&global_lock, true)) {
            printf_err("Failed to release the global lock");
            goto ERROR_UMOUNT;
        }
    }
    else
        release_lock(&global_lock, false);

    return 0;

    //rollback in case of error
ERROR_UMOUNT:
    umount2(runtime_dir_path, UMOUNT_NOFOLLOW | MNT_DETACH);

ERROR_RELEASE:
    release_lock(&global_lock, false);

    return -1;
}

static int mount_box(struct box_runtime* runtime, size_t nbindings, struct binding* bindings, bool exclusive) {
    const char* box_name = runtime->box_name;
    const char* canonical_box_root = runtime->box_root;
    size_t nfilesyncs = runtime->nfilesyncs;
    struct binding* filesyncs = runtime->filesyncs;
    struct timespec* timestamp = &(runtime->timestamp);

    //get lock
    struct lock* lock;
    if (get_lock(&lock, box_name, exclusive, true)) {
        printf_err("Failed to acquire the box lock");
        return -1;
    }

    //mount requested paths
    size_t i = 0;
    if (lock->old_value == 0) {
        for (; i < nbindings; i++)
            if (recursive_bind(bindings[i].source, bindings[i].target)) {
                printf_err("Failed to mount a binding");
                goto ERROR_UMOUNT;
            }
        for (i = 0; i < nfilesyncs; i++)
            if (syncfile_to_box(filesyncs[i].source, filesyncs[i].target, timestamp)) {
                printf_err("Failed to synchronize a file");
                goto ERROR_UMOUNT;
            }
    }

    //set lock value
    if (release_lock(&lock, true)) {
        printf_err("Failed to release the box lock");
        goto ERROR_UMOUNT;
    }
    return 0;

    //rollback in case of error
ERROR_UMOUNT:
    recursive_umount(canonical_box_root);

    release_lock(&lock, false);
    return -1;
}

static void unmount_box(struct box_runtime* runtime) {
    const char* box_name = runtime->box_name;
    const char* canonical_box_root = runtime->box_root;
    bool exclusive = runtime->install;
    size_t nfilesyncs = runtime->nfilesyncs;
    struct binding* filesyncs = runtime->filesyncs;
    struct timespec timestamp = runtime->timestamp;

    //get lock
    struct lock* lock;
    if (get_lock(&lock, box_name, exclusive, false)) {
        printf_err("Failed to acquire the box lock");
        return;
    }

    //umount
    if (lock->new_value == 0) {
        recursive_umount(canonical_box_root);
        for (size_t i = 0; i < nfilesyncs; i++)
            syncfile_to_host(filesyncs[i].target, filesyncs[i].source, timestamp);
    }

    //release lock
    release_lock(&lock, true);
}

void make_bindings(size_t* nbindings, struct binding** bindings, struct box_config* config, bool runtime, bool install, bool filesync) {
    char* path_prefix = config->box_path;
    *nbindings = 0;
    *bindings = NULL;

    if (runtime && !filesync) {
        *nbindings = 1;
        *bindings = malloc(sizeof(struct binding));
        (*bindings)[0].source = alstrcpy(config->box_path);
        (*bindings)[0].target = path_prefix = alsprintf("%s/%s", runtime_dir_path, config->box_name);
    }

    for (size_t i = 0; i < config->nbindings; i++) {
        if (config->bindings[i].filesync != filesync)
            continue;

        if (config->bindings[i].install_only && !install)
            continue;

        size_t n = ++(*nbindings);
        *bindings = realloc(*bindings, n * sizeof(struct binding));
        (*bindings)[n - 1].source = alstrcpy(config->bindings[i].source);
        (*bindings)[n - 1].target = alsprintf("%s/%s", path_prefix, config->bindings[i].target);
    }
}

void free_bindings(size_t nbindings, struct binding* bindings) {
    for (size_t i = 0; i < nbindings; i++) {
        free(bindings[i].source);
        free(bindings[i].target);
    }

    free(bindings);
}

static void free_runtime(struct box_runtime* runtime) {
    free(runtime->box_root);
    free(runtime->box_name);
    free_bindings(runtime->nfilesyncs, runtime->filesyncs);

    free(runtime);
}

int start_box(struct box_runtime** runtime_ptr, struct box_config* config, bool install) {
    if (access(config->box_path, F_OK)) {
        printf_err("This box does not exist");
        return -1;
    }

    //prepare bindings
    size_t nbindings;
    struct binding* bindings;
    make_bindings(&nbindings, &bindings, config, true, install, false);

    //allocate and fill box_runtime structure
    struct box_runtime* runtime = *runtime_ptr = malloc(sizeof(struct box_runtime));
    runtime->box_root = alstrcpy(bindings[0].target);
    runtime->box_name = alstrcpy(config->box_name);
    runtime->install = install;
    runtime->timestamp.tv_sec = runtime->timestamp.tv_nsec = 0;
    make_bindings(&(runtime->nfilesyncs), &(runtime->filesyncs), config, true, install, true);

    //block almost all signals
    sigset_t new_mask;
    sigfillset(&new_mask);
    if (sigprocmask(SIG_SETMASK, &new_mask, &runtime->saved_signal_mask)) {
        printf_syserr("Failed to set the signal mask");
        goto ERROR_FREE_RUNTIME;
    }

    //set as subreaper
    if (prctl(PR_SET_CHILD_SUBREAPER, 1)) {
        printf_syserr("Failed to set the process as a subreaper");
        goto ERROR_RESTORE_SIGNALS;
    }

    //prepare box runtime directory
    if (prepare_global_runtime_directory() || mkdir_all(bindings[0].target)) {
        printf_err("Failed to prepare the runtime directory");
        goto ERROR_RESTORE_SIGNALS;
    }

    //mount bindings (if not mounted)
    if (mount_box(runtime, nbindings, bindings, install)) {
        printf_err("Failed to mount the box");
        goto ERROR_DISABLE_SUBREAPER;
    }

    free_bindings(nbindings, bindings);
    return 0;

    //rollback in case of error
ERROR_DISABLE_SUBREAPER:
    prctl(PR_SET_CHILD_SUBREAPER, 0);

ERROR_RESTORE_SIGNALS:
    sigprocmask(SIG_SETMASK, &runtime->saved_signal_mask, NULL);

ERROR_FREE_RUNTIME:
    free_runtime(runtime);
    *runtime_ptr = NULL;
    free_bindings(nbindings, bindings);

    return -1;
}

int prepare_child_process(struct box_runtime *runtime) {
    //restore signal mask
    if (sigprocmask(SIG_SETMASK, &runtime->saved_signal_mask, NULL)) {
        printf_syserr("Failed to set the signal mask");
        return -1;
    }

    return 0;
}

int stop_box(struct box_runtime** runtime_ptr, pid_t main_child_pid){
    struct box_runtime* runtime = *runtime_ptr;

    //wait for children to terminate
    pid_t pid;
    int wstatus, return_code = 0;
    while ((pid = wait(&wstatus)) != -1 || errno == EINTR)
        if (main_child_pid > 0 && pid == main_child_pid)
            return_code = WIFEXITED(wstatus) ? WEXITSTATUS(wstatus) : -1;

    //umount bindings (if last instance of the box)
    unmount_box(runtime);

    //disable subreaper mode
    prctl(PR_SET_CHILD_SUBREAPER, 0);

    //restore signal mask
    sigprocmask(SIG_SETMASK, &runtime->saved_signal_mask, NULL);

    //free box_runtime structure
    free_runtime(runtime);
    *runtime_ptr = NULL;

    return return_code;
}
