#define _GNU_SOURCE

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mount.h>
#include <sys/wait.h>
#include <sys/prctl.h>
#include <sys/stat.h>

#include "boxinstall.h"
#include "box.h"
#include "utils.h"

static int create_box_files(struct box_config* config) {
    // translate binding paths
    size_t nbindings;
    struct binding* bindings;
    make_bindings(&nbindings, &bindings, config, false, true, false);

    // create box directory
    if (mkdir_all(config->box_path)) {
        printf_err("Failed to create the box directory");
        goto ERROR;
    }

    // create all directories and files necessary to mount
    for (size_t i = 0; i < nbindings; i++) {
        const char* source = bindings[i].source;
        const char* target = bindings[i].target;

        struct stat st;
        if (stat(source, &st)) {
            printf_syserr("Failed to stat() the binding source %s", source);
            goto ERROR;
        }

        if (S_ISDIR(st.st_mode)) {
            if (mkdir_all(target)) {
                printf_err("Failed to create the binding target directory %s", target);
                goto ERROR;
            }
        } else {
            if (mkdir_all_and_touch(target)) {
                printf_err("Failed to create the binding target file %s", target);
                goto ERROR;
            }
        }
    }

    free_bindings(nbindings, bindings);
    return 0;

ERROR:
    free_bindings(nbindings, bindings);
    return -1;
}

static int install_deps_in_box(struct box_config* config, bool noconfirm) {
    size_t nargs = config->ndeps + 1;
    const char** args = malloc(nargs * sizeof(const char*));

    args[0] = "--sync";
    for (size_t i = 0; i < config->ndeps; i++)
        args[i + 1] = config->deps[i];

    int ret = run_pacman_in_box(config, nargs, args, noconfirm);

    free(args);
    return ret;
}

int create_box(struct box_config* config, bool noconfirm) {
    if (!access(config->box_path, F_OK)) {
        printf_err("This box already exists");
        return -1;
    }

    int status = create_box_files(config);
    if (!status)
        status = install_deps_in_box(config, noconfirm);
    return status;
}

static const char** make_pacman_argv(struct box_runtime* runtime, size_t nargs, const char* const* args, bool noconfirm) {
    const char** argv = malloc((nargs + (noconfirm ? 5 : 4)) * sizeof(const char*));

    size_t k = 0;
    argv[k++] = "pacman";
    argv[k++] = "--root";
    argv[k++] = runtime->box_root;
    if (noconfirm)
        argv[k++] = "--noconfirm";
    for (size_t i = 0; i < nargs; i++)
        argv[k++] = args[i];
    argv[k++] = NULL;

    return argv;
}

int run_pacman_in_box(struct box_config* config, size_t nargs, const char* const* args, bool noconfirm) {
    // start box
    struct box_runtime* runtime;
    if (start_box(&runtime, config, true)) {
        printf_err("Failed to start the box");
        return -1;
    }

    // prepare null-terminated argument list
    const char** argv = make_pacman_argv(runtime, nargs, args, noconfirm);

    // run pacman
    pid_t pid = fork();

    if (pid == -1) {
        printf_syserr("Failed to fork()");
        stop_box(&runtime, 0);
        goto ERROR;
    }

    if (pid == 0) {
        // in child process
        if (prepare_child_process(runtime)) {
            printf_err("Failed to prepare the child process");
            exit(-1);
        }

        execvp("pacman", (char**)argv); // this cast should be safe according to POSIX
        printf_syserr("Failed to run pacman");
        exit(-1);
    }

    // stop box
    int exit_code = stop_box(&runtime, pid);

    free(argv);
    return exit_code;

ERROR:
    free(argv);
    return -1;
}

int install_in_box(struct box_config* config, const char* pkgname, bool noconfirm) {
    const char* args[] = { "--sync", pkgname };
    return run_pacman_in_box(config, 2, args, noconfirm);
}

int install_local_in_box(struct box_config* config, const char* filename, bool noconfirm) {
    const char* args[] = { "--upgrade", filename };
    return run_pacman_in_box(config, 2, args, noconfirm);
}

int uninstall_from_box(struct box_config* config, const char* pkgname, bool noconfirm) {
    const char* args[] = { "--remove", "--recursive", pkgname };
    return run_pacman_in_box(config, 3, args, noconfirm);
}

int upgrade_in_box(struct box_config* config, bool noconfirm) {
    const char* args[] = { "--noconfirm", "--sync", "--sysupgrade" };
    return run_pacman_in_box(config, 3, args, noconfirm);
}

int remove_box(struct box_config* config) {
    if (access(config->box_path, F_OK)) {
        printf_err("This box does not exist");
        return -1;
    }

    //get exclusive lock
    struct lock* lock = NULL;
    if (get_lock(&lock, config->box_name, true, true) || release_lock(&lock, true)) {
        printf_err("Failed to get an exclusive lock");
        if (lock)
            release_lock(&lock, false);
        return -1;
    }

    //remove box directory
    rmdir_all(config->box_path);

    //release exclusive lock
    if (get_lock(&lock, config->box_name, true, false) || release_lock(&lock, true)) {
        printf_err("Failed to release an exclusive lock");
        if (lock)
            release_lock(&lock, false);
        return -1;
    }

    return 0;
}
