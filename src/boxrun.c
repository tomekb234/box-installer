#define _GNU_SOURCE

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include "boxrun.h"
#include "box.h"
#include "utils.h"

static const char** make_argv(struct box_config* config, const char* exec_path, bool use_config_args, size_t nargs, const char* const* args) {
    size_t cnargs = use_config_args ? config->nargs : 0;
    const char** argv = malloc((cnargs + nargs + 2) * sizeof(const char*));

    argv[0] = exec_path;
    for (size_t i = 0; i < cnargs; i++)
        argv[i + 1] = config->args[i];
    for (size_t i = 0; i < nargs; i++)
        argv[i + cnargs + 1] = args[i];
    argv[cnargs + nargs + 1] = NULL;

    return argv;
}

static const char** make_envp(struct box_config* config) {
    size_t size = 0;
    const char** envp = NULL;

    // copy environment from host
    if (config->copy_env) {
        for (size_t i = 0; environ[i]; i++) {
            envp = realloc(envp, (++size) * sizeof(const char*));
            envp[size - 1] = environ[i];
        }
    }

    for (size_t i = 0; i < config->env_size; i++) {
        const char* item = config->env[i];

        size_t eq = 0;
        while (item[eq] && item[eq] != '=')
            eq++;

        // if an environment item in config has no '=' character,
        // then it is replaced with corresponding environment item from host
        if (!item[eq]) {
            for (size_t i = 0; environ[i]; i++) {
                if (!strncmp(item, environ[i], eq) && environ[i][eq] == '=') {
                    item = environ[i];
                    break;
                }
            }
        }

        // replace existing environment item
        for (size_t i = 0; i < size; i++) {
            if (!strncmp(item, envp[i], eq) && envp[i][eq] == '=') {
                envp[i] = item;
                item = NULL;
                break;
            }
        }

        // append new environment item
        if (item) {
            envp = realloc(envp, (++size) * sizeof(const char*));
            envp[size - 1] = item;
        }
    }

    envp = realloc(envp, (++size) * sizeof(const char*));
    envp[size - 1] = NULL;

    return envp;
}

int run_box(struct box_config* config, const char* exec_path_override, size_t nargs, const char* const* args, const char* workdir_override) {
    const char* exec_path = exec_path_override ? exec_path_override : config->exec_path;
    const char* workdir = workdir_override ? workdir_override : config->workdir;

    const char** argv = make_argv(config, exec_path, !exec_path_override, nargs, args);
    const char** envp = make_envp(config);

    //start box
    struct box_runtime* runtime;
    if (start_box(&runtime, config, false)) {
        printf_err("Failed to start the box");
        goto ERROR;
    }

    //run the desired program
    pid_t child_pid = fork();

    if (child_pid == -1) {
        printf_syserr("Failed to fork()");
        stop_box(&runtime, 0);
        goto ERROR;
    }

    else if (child_pid == 0) {
        //in child process
        if (prepare_child_process(runtime)) {
            printf_err("Failed to prepare the child process");
            exit(-1);
        }
        if (chroot(runtime->box_root)) {
            printf_syserr("Failed to chroot(%s)", runtime->box_root);
            exit(-1);
        }
        if (chdir(workdir)) {
            printf_syserr("Failed to chdir(%s)", workdir);
            exit(-1);
        }
        if (setuid(getuid())) {
            printf_syserr("Failed to setuid()");
            exit(-1);
        }

        execve(exec_path, (char**)argv, (char**)envp); // these casts should be safe according to POSIX
        printf_syserr("Failed to run the executable %s", exec_path);
        exit(-1);
    }

    //stop box
    int exit_code = stop_box(&runtime, child_pid);

    free(argv);
    free(envp);
    return exit_code;

ERROR:
    free(argv);
    free(envp);
    return -1;
}
