#include <stdio.h>

#include "cli.h"
#include "utils.h"

// print
void print_duplicate(const char* name, const char* option) {
    printf_err("%s: Option '%s' duplicated.", name, option);
}

void print_missing_value(const char* name, const char* option) {
    printf_err("%s: Missing value '%s'.", name, option);
}

void print_no_args(const char* name) {
    printf_err("%s: No arguments passed.", name);
    print_use_help(name);
}

void print_unknown_option(const char* name, const char* option) {
    printf_err("%s: Unknown option '%s'.", name, option);
    print_use_help(name);
}

void print_use_help(const char* name) {
    printf_err("%s: Use --help to display the usage.", name);
}

// check
int check_option_errors(int cnt, int argc, const char* val, const char* name, const char* option) {
    if (cnt >= argc) {
        print_missing_value(name, option);
        return -1;
    }
    if (val) {
        print_duplicate(name, option);
        return -1;
    }
    return 0;
}
