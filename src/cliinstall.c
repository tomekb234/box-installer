#include <stdio.h>
#include <string.h>

#include "cliinstall.h"
#include "cli.h"
#include "version.h"

static const char* help_message =
"Box installer " BOX_INSTALLER_VERSION "\n"
"\n"
"Create a box and install a package with the same name in it\n"
"Usage: box-install <box-name>\n"
"\n"
"Manage boxes\n"
"Usage: box-install <option> <box-name>\n"
"  -c, --create\t\tcreate a box (and install the default dependencies)\n"
"  -r, --remove\t\tremove a box\n"
"\n"
"Manage packages\n"
"Usage: box-install <box-name> <option> [package-name|file-name|pacman-arguments]\n"
"  -i, --install\t\tinstall a package in a box\n"
"  -I, --install-local\tinstall a package from a file in a box\n"
"  -u, --uninstall\tuninstall a package from a box\n"
"  -g, --upgrade\t\tupgrade all packages in a box\n"
"  -p, --pacman\t\trun a pacman command in a box\n"
"\n"
"Use -y or --noconfirm option to automatically accept defaults.\n"
"Use -- to provide box-name starting with '-' character.\n"
"Use -h or --help to display this message.\n"
"\n";

struct install_option {
    char* name;
    int (*func_ptr)(int*, int, const char* const*, struct install_args*);
};

// print
static void print_help() {
    printf("%s", help_message);
}

static int check_option_duplicate(enum install_action action, const char* name, const char* option) {
    if (action != CREATE_AND_INSTALL) {
        print_duplicate(name, option);
        return -1;
    }
    return 0;
}

// parse
static int parse_box_name(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    if (check_option_errors(*arg_cnt, argc, args->box_name, argv[0], "box-name"))
        return -1;

    args->box_name = argv[*arg_cnt];
    (*arg_cnt)++;
    return 0;
}

static int parse_package(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    if (check_option_errors(*arg_cnt, argc, args->package_name, argv[0], "package"))
        return -1;

    args->package_name = argv[*arg_cnt];
    (*arg_cnt)++;
    return 0;
}

static int parse_install(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    if (check_option_duplicate(args->action, argv[0], "action flag - install"))
        return -1;

    args->action = INSTALL;
    return parse_package(arg_cnt, argc, argv, args);
}

static int parse_install_local(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    if (check_option_duplicate(args->action, argv[0], "action flag - install local") ||
            check_option_errors(*arg_cnt, argc, args->file_name, argv[0], "file"))
        return -1;

    args->action = INSTALL_LOCAL;
    args->file_name = argv[*arg_cnt];
    (*arg_cnt)++;
    return 0;
}

static int parse_uninstall(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    if (check_option_duplicate(args->action, argv[0], "action flag - uninstall"))
        return -1;

    args->action = UNINSTALL;
    return parse_package(arg_cnt, argc, argv, args);
}

static int parse_upgrade(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    if (check_option_duplicate(args->action, argv[0], "action flag - upgrade"))
        return -1;

    args->action = UPGRADE;
    return 0;
}

static int parse_create(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    if (check_option_duplicate(args->action, argv[0], "action flag - create"))
        return -1;

    args->action = CREATE;
    return 0;
}

static int parse_remove(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    if (check_option_duplicate(args->action, argv[0], "action flag - remove"))
        return -1;

    args->action = REMOVE;
    return 0;
}

static int parse_pacman(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    if (check_option_duplicate(args->action, argv[0], "action flag - pacman"))
        return -1;

    args->action = PACMAN;
    args->nargs = argc - (*arg_cnt);
    args->args = &(argv[*arg_cnt]);
    (*arg_cnt) = argc;
    return 0;
}

static int parse_no_confirm(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    if (args->noconfirm) {
        print_duplicate(argv[0], "no-confirm");
        return -1;
    }

    args->noconfirm = true;
    return 0;
}

static int parse_help(int* arg_cnt, int argc, const char* const* argv, struct install_args* args) {
    print_help();
    return -1;
}

int parse_install_args(int argc, const char* const* argv, struct install_args* args) {
    args->box_name = NULL;
    args->package_name = NULL;
    args->file_name = NULL;
    args->action = CREATE_AND_INSTALL;
    args->noconfirm = false;
    args->nargs = 0;
    args->args = NULL;

    struct install_option options[] = {
        {"-i", parse_install},
        {"--install", parse_install},
        {"-I", parse_install_local},
        {"--install-local", parse_install_local},
        {"-u", parse_uninstall},
        {"--uninstall", parse_uninstall},
        {"-g", parse_upgrade},
        {"--upgrade", parse_upgrade},
        {"-p", parse_pacman},
        {"--pacman", parse_pacman},
        {"-c", parse_create},
        {"--create", parse_create},
        {"-r", parse_remove},
        {"--remove", parse_remove},
        {"-y", parse_no_confirm},
        {"--noconfirm", parse_no_confirm},
        {"--", parse_box_name},
        {"-h", parse_help},
        {"--help", parse_help},
        {NULL, NULL}
    };

    if (argc == 1) {
        print_no_args(argv[0]);
        return -1;
    }

    int arg_cnt = 1;
    while (arg_cnt < argc) {
        size_t i = 0;
        while (options[i].name && strcmp(options[i].name, argv[arg_cnt]) != 0)
            i++;

        if (options[i].name) {
            arg_cnt++;
            if (options[i].func_ptr(&arg_cnt, argc, argv, args))
                return -1;
        }
        else if (argv[arg_cnt][0] == '-') {
            print_unknown_option(argv[0], argv[arg_cnt]);
            return -1;
        }
        else if (parse_box_name(&arg_cnt, argc, argv, args)) {
            return -1;
        }
    }

    if (!args->box_name) {
        print_missing_value(argv[0], "box-name");
        return -1;
    }

    if (args->action == CREATE_AND_INSTALL) {
        args->package_name = args->box_name;
    }

    return 0;
}
