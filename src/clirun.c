#include <stdio.h>
#include <string.h>

#include "clirun.h"
#include "cli.h"
#include "version.h"

static const char* help_message =
"Box installer " BOX_INSTALLER_VERSION "\n"
"\n"
"Run an executable in a box.\n"
"Usage: box-run [(-w | --working-dir) <path>] [-e | --exec-path] <box-name> [arguments]\n"
"  -w, --working-dir\t\toverride the working directory\n"
"  -e, --exec-path\t\toverride the executable path by treating the first argument as it\n"
"\n"
"Use -- to provide box-name starting with '-' character.\n"
"Use -h or --help to display this message.\n"
"\n";

struct run_option {
    char* name;
    int (*func_ptr)(int*, int, const char* const*, struct run_args*);
};

// print
static void print_help() {
    printf("%s", help_message);
}

// parse
static int parse_help(int* arg_cnt, int argc, const char* const* argv, struct run_args* args) {
    print_help();
    return -1;
}

static int parse_working_dir(int* arg_cnt, int argc, const char* const* argv, struct run_args* args) {
    if (check_option_errors(*arg_cnt, argc, args->working_dir, argv[0], "working-dir"))
        return -1;

    args->working_dir = argv[*arg_cnt];
    (*arg_cnt)++;
    return 0;
}

static int parse_exec_path(int* arg_cnt, int argc, const char* const* argv, struct run_args* args) {
    if (args->exec_path) {
        print_duplicate(argv[0], "exec-path");
        return -1;
    }

    args->exec_path = "exec-path";
    return 0;
}

static int parse_box_with_args(int* arg_cnt, int argc, const char* const* argv, struct run_args* args) {
    if (check_option_errors(*arg_cnt, argc, NULL, argv[0], "box-name"))
        return -1;

    args->box_name = argv[*arg_cnt];
    (*arg_cnt)++;

    if (args->exec_path) {
        if (*arg_cnt >= argc) {
            print_missing_value(argv[0], "exec-path");
            return -1;
        }

        args->exec_path = argv[*arg_cnt];
        (*arg_cnt)++;
    }

    args->nargs = argc - (*arg_cnt);
    args->args = &(argv[*arg_cnt]);
    (*arg_cnt) = argc;
    return 0;
}

int parse_run_args(int argc, const char* const* argv, struct run_args* args) {
    args->box_name = NULL;
    args->exec_path = NULL;
    args->working_dir = NULL;
    args->args = NULL;
    args->nargs = 0;

    struct run_option options[] = {
        {"-w", parse_working_dir},
        {"--workdir", parse_working_dir},
        {"-e", parse_exec_path},
        {"--exec-path", parse_exec_path},
        {"--", parse_box_with_args},
        {"-h", parse_help},
        {"--help", parse_help},
        {NULL, NULL}
    };

    if (argc == 1) {
        print_no_args(argv[0]);
        return -1;
    }

    int arg_cnt = 1;
    while (arg_cnt < argc) {
        size_t i = 0;
        while (options[i].name && strcmp(options[i].name, argv[arg_cnt]) != 0)
            i++;

        if (options[i].name) {
            arg_cnt++;
            if (options[i].func_ptr(&arg_cnt, argc, argv, args))
                return -1;
        }
        else if (argv[arg_cnt][0] == '-') {
            print_unknown_option(argv[0], argv[arg_cnt]);
            return -1;
        }
        else if (parse_box_with_args(&arg_cnt, argc, argv, args)) {
            return -1;
        }
    }

    if (!args->box_name) {
        print_missing_value(argv[0], "box-name");
        return -1;
    }

    return 0;
}
