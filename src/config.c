#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "utils.h"

const char* config_dir_path = "/etc/box-installer";
const char* config_file_name = "config";
const char* boxconfig_dir_name = "boxes";

// reallocates `*line`, copies the next line from `file` into it
// (joining the following lines if they start with '\'),
// removes trailing '\n' (unless escaped with '\'),
// and returns the length of the line or -1 if end of file is reached
static ssize_t next_line(char** line, char** buf, size_t* bufsize, FILE* file) {
    if (getline(buf, bufsize, file) == -1)
        return -1;

    size_t len = restrcpy(line, *buf);
    bool cut = true;

    while (true) {
        int c = fgetc(file);
        if (c != '\\') {
            ungetc(c, file);
            break;
        }

        cut = false;

        if (getline(buf, bufsize, file) == -1)
            break;

        len += restrcpy_at(line, len, *buf);
        cut = true;
    }

    if (cut && (*line)[len - 1] == '\n')
        (*line)[--len] = 0;

    return len;
}

// if value is not null, checks if `line` starts with `key` followed by ' ';
// if it does, reallocates `*value`, and copies the rest of `line` into it;
// if value is null, checks if line equals key
static bool keyword(const char* line, char** value, const char* key) {
    if (!value)
        return !strcmp(line, key);

    size_t len = strlen(key);

    if (!strncmp(line, key, len) && strlen(line) >= len + 1 && line[len] == ' ') {
        restrcpy(value, line + len + 1);
        return true;
    }

    return false;
}

// subprocedures for `read_config_file`

static void add_argument(struct box_config* config, const char* arg) {
    size_t n = ++config->nargs;
    config->args = realloc(config->args, n * sizeof(const char*));
    config->args[n - 1] = alstrcpy(arg);
}

static void add_env_item(struct box_config* config, const char* arg) {
    size_t n = ++config->env_size;
    config->env = realloc(config->env, n * sizeof(const char*));
    config->env[n - 1] = alstrcpy(arg);
}

static void add_binding(struct box_config* config, const char* path, bool filesync) {
    size_t n = ++config->nbindings;
    config->bindings = realloc(config->bindings, n * sizeof(struct conf_binding));
    config->bindings[n - 1].source = alstrcpy(path);
    config->bindings[n - 1].target = alstrcpy(path);
    config->bindings[n - 1].filesync = filesync;
    config->bindings[n - 1].install_only = false;
}

static void set_last_binding_target(struct box_config* config, const char* path) {
    size_t n = config->nbindings;
    if (n > 0)
        restrcpy(&config->bindings[n - 1].target, path);
}

static void set_last_binding_as_install_only(struct box_config* config) {
    size_t n = config->nbindings;
    if (n > 0)
        config->bindings[n - 1].install_only = true;
}

static void remove_binding(struct box_config* config, const char* path, bool filesync) {
    size_t n = config->nbindings;
    size_t i = 0;

    for (; i < n; i++)
        if (!strcmp(config->bindings[i].source, path) && config->bindings[i].filesync == filesync)
            break;

    if (i >= n)
        return;

    free(config->bindings[i].source);
    free(config->bindings[i].target);

    for (; i + 1 < n; i++)
        config->bindings[i] = config->bindings[i + 1];
    config->nbindings--;
}

static void add_dependency(struct box_config* config, const char* arg) {
    size_t n = ++config->ndeps;
    config->deps = realloc(config->deps, n * sizeof(const char*));
    config->deps[n - 1] = alstrcpy(arg);
}

void read_config_file(struct box_config* config, const char* path) {
    FILE* file = fopen(path, "r");

    if (!file)
        return;

    char* buf = NULL;
    size_t bufsize = 0;
    char* line = NULL;
    char* arg = NULL;

    while (next_line(&line, &buf, &bufsize, file) != -1) {
        if (!line[0] || line[0] == '#')
            continue;

        else if (keyword(line, &arg, "path"))
            restrcpy(&config->box_path, arg);

        else if (keyword(line, &arg, "path-prefix"))
            resprintf(&config->box_path, "%s/%s", arg, config->box_name);

        else if (keyword(line, &arg, "exec"))
            restrcpy(&config->exec_path, arg);

        else if (keyword(line, &arg, "exec-prefix"))
            resprintf(&config->exec_path, "%s/%s", arg, config->box_name);

        else if (keyword(line, &arg, "arg"))
            add_argument(config, arg);

        else if (keyword(line, &arg, "workdir"))
            restrcpy(&config->workdir, arg);

        else if (keyword(line, NULL, "copy-env"))
            config->copy_env = true;

        else if (keyword(line, NULL, "empty-env"))
            config->copy_env = false;

        else if (keyword(line, &arg, "env"))
            add_env_item(config, arg);

        else if (keyword(line, &arg, "bind"))
            add_binding(config, arg, false);

        else if (keyword(line, &arg, "sync"))
            add_binding(config, arg, true);

        else if (keyword(line, &arg, "target"))
            set_last_binding_target(config, arg);

        else if (keyword(line, NULL, "install-only"))
            set_last_binding_as_install_only(config);

        else if (keyword(line, &arg, "dont-bind"))
            remove_binding(config, arg, false);

        else if (keyword(line, &arg, "dont-sync"))
            remove_binding(config, arg, true);

        else if (keyword(line, &arg, "dep"))
            add_dependency(config, arg);

        else
            printf_err("Warning: Invalid line in configuration file %s: %s", path, line);
    }

    free(buf);
    free(line);
    free(arg);
    fclose(file);
}

struct box_config* new_config() {
    struct box_config* config = malloc(sizeof(struct box_config));

    config->box_name = NULL;
    config->box_path = NULL;
    config->exec_path = NULL;
    config->nargs = 0;
    config->args = NULL;
    config->workdir = NULL;
    config->copy_env = false;
    config->env_size = 0;
    config->env = NULL;
    config->nbindings = 0;
    config->bindings = NULL;
    config->ndeps = 0;
    config->deps = NULL;

    return config;
}

struct box_config* load_config(const char* box_name) {
    struct box_config* config = new_config();
    restrcpy(&config->box_name, box_name);

    char* config_file_path = alsprintf("%s/%s", config_dir_path, config_file_name);
    char* boxconfig_file_path = alsprintf("%s/%s/%s", config_dir_path, boxconfig_dir_name, box_name);

    read_config_file(config, config_file_path);
    read_config_file(config, boxconfig_file_path);

    free(config_file_path);
    free(boxconfig_file_path);

    return config;
}

void free_config(struct box_config* config) {
    free(config->box_name);
    free(config->box_path);
    free(config->exec_path);

    for (size_t i = 0; i < config->nargs; i++)
        free(config->args[i]);

    for (size_t i = 0; i < config->env_size; i++)
        free(config->env[i]);

    for (size_t i = 0; i < config->nbindings; i++) {
        free(config->bindings[i].source);
        free(config->bindings[i].target);
    }

    for (size_t i = 0; i < config->ndeps; i++)
        free(config->deps[i]);

    free(config->args);
    free(config->workdir);
    free(config->env);
    free(config->bindings);
    free(config->deps);

    free(config);
}
