#include <stdio.h>
#include <unistd.h>

#include "config.h"
#include "box.h"
#include "boxinstall.h"
#include "cliinstall.h"
#include "utils.h"

static int create_box_and_install(struct box_config* config, const char* pkgname, bool noconfirm);

int main(int argc, const char* const* argv) {
    if (getuid() || geteuid()) {
        printf_err("This command must be run as root");
        return 1;
    }

    struct install_args args;
    if (parse_install_args(argc, argv, &args))
        return 1;

    if (check_box_name(args.box_name))
        return 1;

    struct box_config* config = load_config(args.box_name);
    int status;

    switch (args.action) {
        case CREATE:
            status = create_box(config, args.noconfirm);
            break;

        case CREATE_AND_INSTALL:
            status = create_box_and_install(config, args.package_name, args.noconfirm);
            break;

        case INSTALL:
            status = install_in_box(config, args.package_name, args.noconfirm);
            break;

        case INSTALL_LOCAL:
            status = install_local_in_box(config, args.file_name, args.noconfirm);
            break;

        case UNINSTALL:
            status = uninstall_from_box(config, args.package_name, args.noconfirm);
            break;

        case REMOVE:
            status = remove_box(config);
            break;

        case UPGRADE:
            status = upgrade_in_box(config, args.noconfirm);
            break;

        case PACMAN:
            status = run_pacman_in_box(config, args.nargs, args.args, args.noconfirm);
            break;
    }

    free_config(config);
    return status;
}

static int create_box_and_install(struct box_config* config, const char* pkgname, bool noconfirm) {
    printf("[box-install] Creating box...\n");

    int status = create_box(config, noconfirm);
    if (status)
        return status;

    printf("[box-install] Installing in box...\n");

    status = install_in_box(config, pkgname, noconfirm);

    printf("[box-install] Done.\n");
    return status;
}

