#include <stdio.h>
#include <unistd.h>

#include "config.h"
#include "box.h"
#include "boxrun.h"
#include "clirun.h"
#include "utils.h"

int main(int argc, const char* const* argv) {
    if (geteuid()) {
        printf_err("This command must be run with root as effective user id");
        return 1;
    }

    struct run_args args;
    if (parse_run_args(argc, argv, &args))
        return 1;

    if (check_box_name(args.box_name))
        return 1;

    struct box_config* config = load_config(args.box_name);

    int status = run_box(config, args.exec_path, args.nargs, args.args, args.working_dir);

    free_config(config);

    return status;
}
