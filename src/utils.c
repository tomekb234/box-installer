#define _GNU_SOURCE

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/stat.h>

#include "utils.h"

char* alstrcpy(const char* src) {
    char* str = NULL;
    restrcpy(&str, src);
    return str;
}

size_t restrcpy(char** str, const char* src) {
    return restrcpy_at(str, 0, src);
}

size_t restrcpy_at(char** str, size_t pos, const char* src) {
    size_t len = strlen(src);
    *str = realloc(*str, pos + len + 1);
    strcpy(*str + pos, src);
    return len;
}

static int revsprintf(char** str, const char* format, va_list args1, va_list args2) {
    size_t len = vsnprintf(NULL, 0, format, args1);
    *str = realloc(*str, len + 1);
    return vsprintf(*str, format, args2);
}

char* alsprintf(const char* format, ...) {
    char* str = NULL;

    va_list args1, args2;
    va_start(args1, format);
    va_start(args2, format);
    revsprintf(&str, format, args1, args2);
    va_end(args1);
    va_end(args2);

    return str;
}

int resprintf(char** str, const char* format, ...) {
    va_list args1, args2;
    va_start(args1, format);
    va_start(args2, format);
    int result = revsprintf(str, format, args1, args2);
    va_end(args1);
    va_end(args2);

    return result;
}

void printf_err(const char* format, ...) {
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    fprintf(stderr, "\n");
    va_end(args);
}

void printf_syserr(const char* format, ...) {
    int saved_errno = errno;
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    fprintf(stderr, ": %s\n", strerror(saved_errno));
    va_end(args);
}

static int mkdir_single(const char* dir) {
    if (mkdir(dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) && errno != EEXIST) {
        printf_syserr("Failed to create directory %s", dir);
        return -1;
    }
    return 0;
}

int mkdir_parents(const char* path) {
    char* pathcpy = alstrcpy(path);
    for (int i = 0; pathcpy[i]; i++) {
        if (i > 0 && pathcpy[i] == '/') {
            pathcpy[i] = 0;
            if (mkdir_single(pathcpy)) {
                free(pathcpy);
                return -1;
            }
            pathcpy[i] = '/';
        }
    }
    free(pathcpy);

    return 0;
}

int mkdir_all(const char* dir) {
    return (mkdir_parents(dir) || mkdir_single(dir)) ? -1 : 0;
}

int touch(const char* file) {
    int fd;
    if ((fd = open(file, O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1) {
        printf_syserr("Failed to create file %s", file);
        return -1;
    }
    return close(fd);
}

int mkdir_all_and_touch(const char* file) {
    return (mkdir_parents(file) || touch(file)) ? -1 : 0;
}

int rmdir_all(const char* path) {
    DIR* dir;
    struct dirent *dent;
    struct stat sb;

    if ((dir = opendir(path)) == NULL) {
        printf_syserr("Failed to open directory %s", path);
        return -1;
    }

    while ((dent = readdir(dir)) != NULL) {
        if (strcmp(dent->d_name, ".") == 0 || strcmp(dent->d_name, "..") == 0)
            continue;

        char* file_path = alsprintf("%s/%s", path, dent->d_name);

        if (lstat(file_path, &sb)) {
            printf_syserr("Failed to stat() file %s", file_path);
            free(file_path);
            closedir(dir);
            return -1;
        }

        if (S_ISDIR(sb.st_mode)) {
            if (rmdir_all(file_path)) {
                free(file_path);
                closedir(dir);
                return -1;
            }
        } else {
            if (unlink(file_path)) {
                printf_syserr("Failed to unlink file %s", file_path);
                free(file_path);
                closedir(dir);
                return -1;
            }
        }

        free(file_path);
    }

    if (closedir(dir)) {
        printf_syserr("Failed to close directory %s", path);
        return -1;
    }

    if (rmdir(path)) {
        printf_syserr("Failed to remove directory %s", path);
        return -1;
    }

    return 0;
}

int copy_file(const char* source, const char* target) {
    int fd_src, fd_tar;
    char buf[4096];
    ssize_t nread;

    if ((fd_src = open(source, O_RDONLY)) == -1) {
        printf_syserr("Failed to open source file %s", source);
        return -1;
    }

    struct stat st;
    if (fstat(fd_src, &st)) {
        printf_syserr("Failed to stat() source file %s", source);
        return -1;
    }

    char* target_new = alsprintf("%s.box.new", target);
    if (mkdir_parents(target_new) || (fd_tar = open(target_new, O_WRONLY | O_CREAT | O_EXCL, 0)) == -1) {
        printf_syserr("Failed to create new file %s", target_new);
        free(target_new);
        close(fd_src);
        return -1;
    }

    if (fchown(fd_tar, st.st_uid, st.st_gid)) {
        printf_syserr("Failed to change the owner of file %s", target_new);
        goto COPY_ERROR;
    }

    if (fchmod(fd_tar, st.st_mode)) {
        printf_syserr("Failed to change the mode of file %s", target_new);
        goto COPY_ERROR;
    }

    while ((nread = read(fd_src, buf, sizeof buf)) > 0) {
        char *out_ptr = buf;
        ssize_t nwritten;

        do {
            nwritten = write(fd_tar, out_ptr, nread);
            if (nwritten >= 0) {
                nread -= nwritten;
                out_ptr += nwritten;
            }
            else if (errno != EINTR) {
                printf_syserr("Failed to write to file %s", target_new);
                goto COPY_ERROR;
            }
        } while (nread > 0);
    }

    if (nread != 0) {
        printf_syserr("Failed to read from source file %s", source);
        goto COPY_ERROR;
    }

    if (touch(target)) {
        printf_err("Failed to touch target file %s", target);
        goto COPY_ERROR;
    }

    if (renameat2(AT_FDCWD, target_new, AT_FDCWD, target, RENAME_EXCHANGE)) {
        printf_syserr("Failed to exchange files %s and %s", target, target_new);
        goto COPY_ERROR;
    }

    if (unlink(target_new)) {
        printf_syserr("Failed to remove file %s", target_new);
        goto COPY_ERROR;
    }

    free(target_new);
    close(fd_tar);
    close(fd_src);

    return 0;

COPY_ERROR:
    close(fd_src);
    close(fd_tar);
    unlink(target_new);
    free(target_new);

    return -1;
}
