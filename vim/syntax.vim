" Vim syntax file
" Language: Box-installer config files
" Latest Revision: 09 January 2022

if exists("b:current_syntax")
	finish
endif

if (v:version == 704 && has("patch-7.4.1142")) || v:version > 704
	syn iskeyword 97-122,45
else
	setl isk=97-122,45
endif

" Argument classes
syn match biArgumentInvalid '.*$' contained
syn match biArgumentAny ' .*$' contained
syn match biArgumentAbsolutePath ' /.*$' contained
syn match biArgumentSingleWord ' [^ ]\+$' contained

" Keywords
syn match biBegin '^'  nextgroup=biKeyword

syn keyword biKeyword path path-prefix exec exec-prefix workdir bind sync target dont-bind dont-sync contained nextgroup=biArgumentInvalid,biArgumentAbsolutePath
syn keyword biKeyword env arg contained nextgroup=biArgumentAny
syn keyword biKeyword dep contained nextgroup=biArgumentInvalid,biArgumentSingleWord
syn keyword biKeyword copy-env empty-env install-only contained nextgroup=biArgumentInvalid

" Comment lines
syn match biLineComment '^#.*$' 
syn match biLineComment '^$' 

" Set colors
let b:current_syntax = "boxinstaller"
hi def link biLineComment Comment
hi def link biArgumentAny String
hi def link biArgumentAbsolutePath String
hi def link biArgumentSingleWord String
hi def link biArgumentInvalid Error
hi def link biKeyword Keyword
